Buckbee Weed Co. is a partner of Sustainable Innovations, Inc. with a product line consisting of vape cartridges, tinctures, vegan gel caps, gummies, flower, and prerolls. Buckbee Weed Co.s line features Delta-8 THC, CBD, CBG, and CBN cannabinoids. Sustainable Innovations, Inc. is the exclusive distributor for Buckbee Weed Co. worldwide.

Website: https://www.buckbeeweed.com/
